import setuptools

setuptools.setup(
    name='milight-api',
    version='0.1.0',
    url='https://gitlab.com/nielsvangijzen/python-milight-api',
    author='Niels van Gijzen',
    author_email='',
    license='MIT',
    packages=['MilightApi']
)